import Vue from "vue";

import { ServerTable } from "vue-tables-2";
import SearchBox from "@/components/common/table/SearchBox";

Vue.use(ServerTable, {}, false, "bootstrap4", {
  VtDataTable: null,
  VtGenericFilter: SearchBox
});
