import Vue from "vue";
import VueRouter from "vue-router";
// Views
import Home from "../views/Home.vue";
const OauthCallback = () => import("../views/OauthCallback.vue");
const Demo = () => import("../views/Demo.vue");
const Dashboard = () => import("../views/Dashboard.vue");
import About from "../views/About.vue";
import Skill from "../views/Skill.vue";
import Prices from "../views/Prices.vue";
import Matcher from "../views/Matcher.vue";
import PrivacyPolicy from "../views/PrivacyPolicy.vue";
import TermsConditions from "../views/TermsConditions.vue";
import CookieDisclaimer from "../views/CookieDisclaimer.vue";
const Universipedia = () => import("../views/Universipedia.vue");
const Geurs = () => import("../views/Geurs.vue");
const WidgetIframe = () => import("../views/WidgetIframe.vue");
const TimeSeries = () => import("../views/TimeSeries2.vue"); // DEBUG REMOVE 2 FOR PRODUCTION

// Utilities
import Settings from "../views/Settings.vue";
import store from "../store";
import { loggedIn, reconnect } from "@/api/user";
import ConnectorEventBus from "@/mixins/ConnectorEventBus";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/demo",
    name: "Demo",
    component: Demo
  },
  {
    path: "/oauthCallback/:provider",
    name: "OauthCallback",
    component: OauthCallback
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard
  },
  {
    path: "/about",
    name: "About",
    component: About
  },
  {
    path: "/prices",
    name: "Prices",
    component: Prices
  },
  {
    path: "/privacy",
    name: "Privacy",
    component: PrivacyPolicy
  },
  {
    path: "/terms",
    name: "Terms",
    component: TermsConditions
  },
  {
    path: "/WidgetIframe",
    name: "WidgetIframe",
    component: WidgetIframe,
    meta: {
      layout: "iframe"
    }
  },
  {
    path: "/cookies",
    name: "Cookies",
    component: CookieDisclaimer
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings
  },
  {
    path: "/matcher",
    name: "Matcher",
    component: Matcher
  },
  {
    path: "/skill/:skill",
    name: "Skill",
    component: Skill,
    props: true
  },
  {
    // UserProfile will be rendered inside User's <router-view>
    // when /user/:id/profile is matched
    path: "/data/univData",
    name: "Universipedia",
    component: Universipedia
  },
  {
    // UserProfile will be rendered inside User's <router-view>
    // when /user/:id/profile is matched
    path: "/data/geurs/:year",
    name: "Geurs",
    component: Geurs
  },
  {
    // UserProfile will be rendered inside User's <router-view>
    // when /user/:id/profile is matched
    path: "/data/timeSeries",
    name: "TimeSeries",
    component: TimeSeries
  }
  //   {
  //     path: '/about',
  //     name: 'About',
  //     // route level code-splitting
  //     // this generates a separate chunk (about.[hash].js) for this route
  //     // which is lazy-loaded when the route is visited.
  //     component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  //   },
];

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  // redirect to home page if not logged in and trying to access a restricted page
  // const privatePages = ["Skill", "Dashboard", "Settings"];
  const privatePages = ["Skill", "Geurs", "Dashboard", "Universipedia", "Settings", "Matcher"];
  const authRequired = privatePages.includes(to.name);

  //   reconnect()
  //     .then(() => {
  //       console.log("Reconnected");
  //     })
  //     .catch(e => {
  //       console.log("problem reconnecting");
  //     });

  // AUTHENTICATED routes
  if (authRequired) {
    if (store.state.loggedIn) next();
    else {
      router.push("/");
    }
  } else next();
});

export default router;
