module.exports = {
  API_URL:
    process.env.NODE_ENV === "production" ? "https://e360api.pollup.net" : "http://127.0.0.1:5000",
  TEST_USER: {
    email: process.env.NODE_ENV === "production" ? "" : "thomas",
    password: process.env.NODE_ENV === "production" ? "" : "tompouce"
  },
  STRAPI_URL: "https://heepstrapi.pollup.net",
  NEO4J_URL:
    process.env.NODE_ENV === "production"
      ? "https://neo4j.pollup.net/db/data"
      : "http://127.0.0.1:7474/db/data",
  NEO4J_TOKEN:
    process.env.NODE_ENV === "production" ? "bmVvNGo6VG9tcG91Y2UxMg==" : "bmVvNGo6VG9tcG91Y2UxMg==", // user:pass Base64 encoded
  cacheExpires: process.env.NODE_ENV === "production" ? 24 * 3600 * 1000 : 1000 * 10,
  oauth: {
    domain: "dev-moquy53y.eu.auth0.com",
    clientId: "mxDwajsf2lhulTvaA78lbCdFMX0PXMp8"
  }
};
