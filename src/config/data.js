module.exports = {
  drivers: {
    2019: [
      "Internationality",
      "Practical relevance / professionalization",
      "Soft skills and digital literacy",
      "Specialization",
      "Academic excellence"
    ],
    2020: [
      "Academic performance",
      "Specialisation",
      "Graduate skills",
      "Focus on work expertise",
      "Digital performance",
      "Internationality"
    ]
  },
  defaultUniversities: {
    Sorbonne: [
      {
        label: "Sorbonne University - Sciences",
        id: "5c9e895334cda4f3677e7141"
      },
      {
        label: "University of Cambridge",
        id: "5c9e636734cda4f3677e5027"
      },
      {
        label: "University of Montreal / HEC",
        id: "5c9e498a34cda4f3677e32d9"
      },
      {
        label: "Ecole Polytechnique Fédérale de Lausanne",
        id: "5c9e355234cda4f3677e11d1"
      },
      {
        label: "Mines ParisTech (PSL Université)",
        id: "5c9e5bc734cda4f3677e43a7"
      }
    ],
    Erasmus: [
      //Erasmus University Rotterdam -  competitors:  University of Amsterdam, Leiden University, University of Bergen, Mac Gill University,  Eindhoven University of Technology//
      {
        label: "Erasmus University Rotterdam",
        id: "5c9e349434cda4f3677e109f"
      },
      {
        label: "University of Amsterdam",
        id: "5c9e644f34cda4f3677e519d"
      },
      {
        label: "Leiden University",
        id: "5c9e645534cda4f3677e51a7"
      },
      {
        label: "University of Bergen",
        id: "5c9e67db34cda4f3677e56e9"
      },
      {
        label: "McGill University",
        id: "5c9e5c6f34cda4f3677e44bd"
      },
      {
        label: "Eindhoven University of Technology",
        id: "5c9e7e9c34cda4f3677e6d15"
      }
    ],
    Monterrey: [
      //  Monterrey Institute of Technology - Competitors: University of Montreal/Hec, National Autonomous University of Mexico, University of British Columbia, University of Sao Paulo ( ou une argentine, Thomas peut choisir), University of Navarra
      {
        label: "Monterrey Institute of Technology",
        id: "5c9e7e6234cda4f3677e6cbb"
      },
      {
        label: "University of Montreal/HEC",
        id: "5c9e498a34cda4f3677e32d9"
      },
      {
        label: "National Autonomous University of Mexico",
        id: "5c9e70cf34cda4f3677e623d"
      },
      {
        label: "University of British Columbia",
        id: "5c9e7d8334cda4f3677e6b4f"
      },
      {
        label: "University of Buenos Aires",
        id: "5c9e7a6834cda4f3677e662b"
      },
      {
        label: "University of Navarra",
        id: "5c9e79b334cda4f3677e6503"
      }
    ],
    epfl: [
      {
        label: "École Polytechnique Fédérale de Lausanne",
        id: "5c9e355234cda4f3677e11d1"
      },
      {
        label: "ETH Zurich",
        id: "5c9e34d534cda4f3677e1107"
      },
      {
        label: "University of Oxford",
        id: "5c9e61c234cda4f3677e4d7b"
      },
      {
        label: "Massachusetts Institute of Technology",
        id: "5c9e5c9634cda4f3677e44fb"
      },
      {
        label: "École Polytechnique",
        id: "5c9e355634cda4f3677e11d7"
      }
    ]
  },
  statements: {
    "2019": [
      {
        text: "Hire for attitude - train for skill",
        questionId: "Q014_001"
      },
      {
        text: "Universities should focus on long-term / life-long employability of their students.",
        questionId: "Q014_002"
      },
      {
        text: "Universities should mainly prepare students to slot into the present labour market.",
        questionId: "Q014_003"
      },
      {
        text: "Universities can’t be sustainable without being socially responsible.",
        questionId: "Q014_004"
      },
      {
        text: "Higher education has become globalized.",
        questionId: "Q014_005"
      },
      {
        text: "Universities must increasingly compete in the market place.",
        questionId: "Q014_006"
      },
      {
        text: "Universities need to collaborate with companies.",
        questionId: "Q014_007"
      }
    ],
    "2020": [
      {
        text:
          "The corona crisis will force universities to adjust radically and review their learning programs.",
        questionId: "Q4F_1"
      },
      {
        text: "The corona crisis will upset the established global university order.",
        questionId: "Q4F_2"
      },
      {
        text:
          "The competencies companies need from graduates will evolve strongly due to the corona crisis. ",
        questionId: "Q4F_3"
      },
      {
        text: "Online education can replace face-to-face training programs.",
        questionId: "Q4F_4"
      },
      {
        text: "Universities have adjusted well so far to the challenges of the corona crisis. ",
        questionId: "Q4F_5"
      },
      {
        text:
          "Companies should focus on collaborating with universities to develop significantly new digital training formats.",
        questionId: "Q4F_6"
      }
    ]
  }
};
