const _ = require("lodash");

module.exports = {
  formatNumber: function(i, locale = undefined) {
    var formatter = new Intl.NumberFormat(locale);
    return formatter.format(i);
  },
  formatPercent: function(value, decimal = 2, multiply = true, suffix = "%") {
    value = value == null ? 0 : value;
    if (multiply) value = value * 100;
    return value.toFixed(decimal) + suffix;
  },
  formatPercentNumber: function(value, decimal = 2) {
    return (value * 100).toFixed(decimal);
  },
  percent: function(value, total, decimal = 2) {
    let val = total == 0 ? 0 : (value / total) * 100;
    return val.toFixed(decimal);
  },
  /**
   * Utility to sort an array of object by the value of one of his properties
   * @param  {object or array} res
   * @param  {Array or function} property
   * @param (String) special cases
   * @return sorted array of object according to property
   * see doc: https://lodash.com/docs/4.17.15#sortBy
   */
  sortArrayOfObject: function(ar, property, special = "") {
    if (special == "satisfaction") {
      return _.orderBy(
        ar,
        elt => {
          if (elt[property].includes("Not")) return 1;
          else if (elt[property].includes("Very")) return 10;
          else return parseInt(elt[property]);
        },
        "asc"
      );
    }
    return _.sortBy(ar, property);
  },

  shortenText: function(str, len, chars = "...") {
    return _.truncate(str, {
      length: len,
      omission: chars
    });
  },

  splitText: function(str, minLen = 35) {
    return str.match(/.{1,35}(\s|$)/g);
  },
  /**
   *
   * @param  {Object} obj: the Array to mutate
   *                [{original_property1: val1, original_property2: val2}, {original_property1: val3, original_property2: val4}]
   * @param  {Object} mappings: mappings of property transformations
   *                    { original_property1: new_property1, original_property2: new_property2, ...}
   *
   * @return
   */
  changeProperties: function(ar, mappings) {
    let ret = {};
    ret = ar.map(obj => {
      let o = {};
      Object.keys(mappings).forEach(m => {
        o[m] = obj[mappings[m]];
      });
      return o;
    });
    return ret;
  },

  prepareData: function(data) {
    let start = data.page * data.limit - data.limit;
    let end = parseInt(start) + parseInt(data.limit);
    let search = data.query;
    let sort = `${data.byColumn} ${data.ascending == 1 ? "ASC" : "DESC"}`;
    return `start=${start}&end=${end}&search=${search}&sort=${sort}`;
  },

  fixDecimal: function(str) {
    //str = str.replace(".", ",")
    return parseFloat(str);
  },

  subRoutes: function(routes, name) {
    // Find recursively member who's name property is given
    let route = _.find(routes.options.routes, { name });
    if (!route) {
      console.log("No route for: ", name);
      return false;
    }
    route.children.map(obj => (obj.path = route.path + obj.path));
    return route.children;
  },
  divideIntoSeries(ar, fields) {
    let rem = {};
    fields.forEach(field => {
      let ret = [];
      ar.forEach(o => {
        let m = _.clone(o);
        let r = {
          x: m._id,
          y: m[field],
          label: m._id
        };
        ret.push(r);
      });
      console.log("RET", ret);
      rem[field] = ret;
    });
    console.log("REMSSS", rem);
    return rem;
  },

  renameProperty(ar, mapper) {
    let remapper = item =>
      _(item) // lodash chain start
        .mapKeys((v, k) => mapper[k] || k)
        .value(); // lodash chain end

    return ar.map(remapper);
  },

  filtersId(prefix, n = 0) {
    if (n <= 1 || n == undefined) return prefix;
    else {
      let names = [];
      for (let i = 0; i < n; i++) {
        names[i] = `${prefix}_##_${i}_`;
      }
      return names;
    }
  },

  sortY(data, options) {
    if (options.sort.order === "alpha") return data.sort((a, b) => a.y.localeCompare(b.y));
    else
      return data.sort((a, b) => {
        return parseFloat(b.y) - parseFloat(a.y);
      });
  },
  hasValues(obj) {
    return !_.values(obj).every(_.isEmpty) || !_.values(obj).every(v => v == undefined);
  },
  getAveragePoints(points) {
    return points.reduce((total, next) => total + next.y, 0) / points.length;
  },

  // Common data
  filtersDefinition(type) {
    const filtersSuffix = {
      countryMultiple: "COUNTRY",
      countrySingle: "COUNTRY",
      UniversitySelector: "UNIV",
      role: "ROLE",
      sector: "SECTOR",
      department: "DEPARTMENT",
      companySize: "EMPLOYEE_NUMBER"
    };
    const data = [...new Set(Object.values(filtersSuffix))];
    return type == "complete" ? filtersSuffix : data;
  }
};
