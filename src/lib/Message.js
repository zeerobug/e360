import store from "@/store";

class Message {
  constructor(data) {
    if (data.subscribed) {
      this.body = this.renderBody(data.body, data.subscribed);
    } else this.body = data.body;
    this.title = data.title;
    this.emphasis = data.emphasis ? data.emphasis : "";
    this.selected = data.selected ? data.selected : false;
    this.link = data.link ? data.link : "";
    this.component = data.component ? data.component : "";
    this.returnFocus = data.returnFocus ? data.returnFocus : "";
  }

  renderBody(body, subscribed) {
    // Todo make it better
    return body.replace(`{{${subscribed}}}`, store.state.cache[subscribed]);
  }
}
export default Message;
