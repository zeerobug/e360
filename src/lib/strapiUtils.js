const _ = require("lodash");

module.exports = {
    parseErrorMessage: function (e) {
        let resp = [];
        e.data.message.forEach(msg => {
            msg.messages.forEach(m => { 
                resp.push(m.message)
            })
        })
        return resp.join("<br/>");
    }
}