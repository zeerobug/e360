let _ = require("lodash");
class graphUtils {
  constructor() {
    this.palette_strong = ["#C7E84D", "#6FDAD9", "#61B9D6", "#3A7488"];
    this.fluo = [
      { name: "Radical Red", value: "#FF355E" },
      { name: "Wild Watermelon", value: "#FD5B78" },
      { name: "Outrageous Orange", value: "#FF6037" },
      { name: "Atomic Tangerine", value: "#FF9966" },
      { name: "Neon Carrot", value: "#FF9933" },
      { name: "Sunglow", value: "#FFCC33" },
      { name: "Laser Lemon", value: "#FFFF66" },
      { name: "Unmellow Yellow", value: "#FFFF66" },
      { name: "Electric Lime", value: "#CCFF00" },
      { name: "Screamin' Green", value: "#66FF66" },
      { name: "Magic Mint", value: "#AAF0D1" },
      { name: "Blizzard Blue", value: "#50BFE6" },
      { name: "Shocking Pink", value: "#FF6EFF" },
      { name: "Razzle Dazzle Rose", value: "#EE34D2" },
      { name: "Hot Magenta", value: "#FF00CC" },
      { name: "Purple Pizzazz", value: "#FF00CC" }
    ];
    this.largePalette = [
      "#4ecdc4",
      "#c7f464",
      "#ff6b6b",
      "#c44d58",
      "#733cd8",
      "#1242aa",
      "#dbbb53",
      "#29d18e",
      "#bc1ed8",
      "#11d6c2",
      "#8ee550",
      "#bc2729",
      "#f2da93",
      "#a0d7db",
      "#d5dfc4",
      "#f0e7b1",
      "#f99417",
      "#fd7500",
      "#f6c270",
      "#ffcdae",
      "#08427b",
      "#ee98f3",
      "#15a900",
      "#d0eb3a",
      "#9dc5a8",
      "#c9e420",
      "#a1eb65"
    ];
    this.paletteFluo = this.fluo.map(o => o.value);
    this.palette = [
      ...this.palette_strong,
      "rgb(199, 232, 77, 0.3)",
      "rgb(229, 132, 56, 0.3)",
      "rgb(116, 221, 222, 0.3)",
      "#ff5151",
      "#5b8c5a"
    ];
    // let palette_soft = ["#8EC4D5"];
    this.areaPalette = [
      {
        backgroundColor: "rgba(188, 221, 71,0.1)",
        borderColor: "rgb(199, 232, 77)",
        borderWidth: 2,
        pointRadius: 2,
        type: "radar"
      },
      {
        backgroundColor: "rgba(229, 132, 56, 0.1)",
        borderColor: "rgb(229, 132, 56)",
        borderWidth: 2,
        pointRadius: 2
      },
      {
        backgroundColor: "rgba(116, 221, 222, 0.1)",
        borderColor: "rgb(116, 221, 222)",
        borderWidth: 2,
        pointRadius: 2
      },
      {
        backgroundColor: "rgba(116, 221, 222, 0.0)",
        borderColor: "grey",
        borderWidth: 1,
        pointRadius: 2
      }
    ];
    this.radarPalette = [
      {
        backgroundColor: "rgba(188, 221, 71,0.4)",
        borderColor: "rgb(199, 232, 77)",
        borderWidth: 2,
        pointRadius: 2,
        type: "radar"
      },
      {
        backgroundColor: "rgba(229, 132, 56, 0.4)",
        borderColor: "rgb(229, 132, 56)",
        borderWidth: 2,
        pointRadius: 2,
        type: "radar"
      },
      {
        backgroundColor: "rgba(116, 221, 222, 0.4)",
        borderColor: "rgb(116, 221, 222)",
        borderWidth: 2,
        pointRadius: 2,
        type: "radar"
      },
      {
        backgroundColor: "rgba(116, 221, 222, 0.4)",
        borderColor: "grey",
        borderWidth: 1,
        pointRadius: 2,
        type: "radar"
      }
    ];

    this.normalPalette = [
      {
        spanGaps: false,
        borderColor: "rgb(199, 232, 77)",
        borderWidth: 2,
        pointRadius: 2,
        lineTension: 0,
        fill: false
      },
      {
        spanGaps: false,
        borderColor: "rgb(229, 132, 56)",
        borderWidth: 2,
        pointRadius: 2,
        lineTension: 0,
        fill: false
      },
      {
        spanGaps: false,
        borderColor: "rgb(116, 221, 222)",
        borderWidth: 2,
        pointRadius: 2,
        fill: false
      },
      {
        spanGaps: false,
        borderColor: "#ff5151",
        borderWidth: 2,
        pointRadius: 2,
        fill: false
      },
      {
        spanGaps: false,
        borderColor: "#5b8c5a",
        borderWidth: 2,
        pointRadius: 2,
        fill: false
      }
    ];
  }

  adaptColors(dataSet) {
    //let palette = this.normalPalette;
    let palette = this.makePoints(this.largePalette);
    return dataSet.map((obj, i) => ({ ...obj, options: palette[i % palette.length] }));
  }

  generateColorsFluo(n) {
    let fluo = _.sortBy(this.fluo, "name");
    if (n <= fluo.length) {
      return fluo.slice(0, n).map(res => res.value);
    } else {
      let res = [];
      for (let index = 0; index < n; index++) {
        res.push(fluo[index % fluo.length].value);
      }
      return res;
    }
  }

  generateColors(n) {
    let pal = this.largePalette;
    if (n <= pal.length) {
      return pal.slice(0, n);
    } else {
      let res = [];
      for (let index = 0; index < n; index++) {
        res.push(pal[index % pal.length]);
      }
      return res;
    }
  }
  generateRandomColors(n) {
    let res = [];
    for (let index = 0; index < n; index++) {
      res.push(this.randomHSL(index));
    }
    return res;
  }
  randomHSL(number) {
    const hue = number * 137.508; // use golden angle approximation
    return `hsl(${hue},50%,60%)`;
  }
  setColors(chart) {
    let palette = this.radarPalette;
    return chart.result && chart.result.series
      ? chart.result.series.map(
          (s, i) => (s.options = { ...s.option, ...palette[i % palette.length] })
        )
      : null;
  }

  makePoints(palette) {
    let res = [];
    for (let i = 0; i < palette.length; i++) {
      res[i] = {
        spanGaps: false,
        borderColor: palette[i],
        color: palette[i],
        borderWidth: 2,
        pointRadius: 2,
        lineTension: 0,
        fill: false
      };
    }
    return res;
  }
}

module.exports = graphUtils;
