const { faColumns } = require("@fortawesome/free-solid-svg-icons");
const { data } = require("jquery");
const _ = require("lodash");

module.exports = {
  objectify: function(dataSet, cols) {
    var res = [];
    let n = 0;
    dataSet.forEach(data => {
      var row = rowid(data.row, cols);
      const val = typeof row === "object" ? Object.assign({}, row) : row;
      res.push(val);
      n++;
    });
    return res;
  },

  objectify2: function(dataSet, cols) {
    let result = [];
    let n = 0;
    for (let i = 0; i < dataSet.length; i++) {
      let vals = {};
      const element = dataSet[i]["row"];
      for (let y = 0; y < cols.length; y++) {
        vals[cols[y]] = element[y];
      }
      result[n++] = vals;
    }
    console.log("RESULT", result);
    return result;
  },
  /**
   *
   * @param  {Array} data array of objects returned by Neo4j
   * @param  {any} The fieldName in object used  as serie name
   * @return {Array} array of Cojasclib series
   */
  serify: function(data, serieName) {
    let names = _.uniq(_.map(data, serieName));
    let series = {
      data: []
    };
    names.forEach(name => {
      let serie = {
        name: name,
        data: data.filter(m => m[serieName] == name)
      };
      series["data"].push(serie);
    });
    return series;
  }
};

function rowid(row, cols) {
  let obj = {};
  for (let index = 0; index < cols.length; index++) {
    var key = cols[index];
    obj[key] = row[index];
  }
  return obj;
}
