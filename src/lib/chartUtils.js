const { assert } = require("chai");
const Cojasclib = require("cojasclib");
const _ = require("lodash");
const { filtersDefinition } = require("@/lib/utils");
const filtersSuffix = filtersDefinition("complete");

var self = (module.exports = {
  render: async function(series, filters, renderOptions, chart) {
    _checkVariable(series, "series");
    _checkVariable([series, renderOptions.legends], "sameLength");
    for (let i = 0; i < series.length; i++) {
      self._addSerie(renderOptions.legends[i], series[i].data, series[i].options, chart);
    }
    let data = await chart.render(renderOptions.plugin, renderOptions);
    let chartData = data[0];
    let options = data[1];
    return { chartData, options };
  },
  _addSerie: function(name, res, options, chart) {
    // Prepare points
    if (!_.isArray(res)) return;
    res.map(p => (p._id ? (p.x = p._id) : p.x));
    let serie = new Cojasclib.Serie({
      name: name,
      data: res,
      options
    });
    chart.setSerie(serie);
  },

  correctPoints: function(data) {
    return data.map(p => (p._id ? (p.x = p._id) : p.x));
  },
  subscribeToFilters: function(filters, id) {
    let subscribeSuffixes = [];
    for (const key in filters) {
      if (Object.hasOwnProperty.call(filters, key)) {
        if (filters[key]) {
          subscribeSuffixes.push(id + filtersSuffix[key]);
        }
      }
    }
    return subscribeSuffixes;
  },
  makeSeries: function(data) {
    let ret = {};
    ret["series"] = [];
    let res = _.groupBy(data, "serie");
    for (const key in res) {
      if (key == "undefined")
        throw new Error("Problem grouping. Object should have a property named series");
      ret.series.push({ name: key, data: res[key] });
    }
    return ret;
  },
  getYearsList: function(from = 2012, to = 2020) {
    let fields = [{ key: "university", label: "University" }];
    for (let index = from; index <= to; index++) {
      fields.push({
        key: index.toString(),
        label: index.toString()
      });
    }
    return fields;
  }
});

function _checkVariable(parameter, type) {
  let good = true;
  let x = 0;
  switch (type) {
    case "series":
      assert(_.isArray(parameter), `Parameter of type ${type} should be an array`);
      break;
    case "sameLength":
      parameter.forEach(ar => {
        x = x === 0 ? ar.length : x;
        if (ar.length != x) good = false;
      });
      assert(good, `Arrays should be the same length`);
      break;
    default:
      throw new Error("Unknown type: " + type);
  }
}
