import store from "@/store";
import SparkMD5 from "spark-md5";

let tvCache = {
  isCached: key => {
    let cached = store.getters.allCached;
    if (!cached[key]) return false;
    else {
      let expires = cached[key].expires;
      if (!tvCache.expired(expires)) {
        let v = JSON.parse(cached[key].value);
        return v;
        //return JSON.parse(cached[key].value));
      } else return false;
    }
  },
  generateKey: s => {
    return SparkMD5.hash(s);
  },
  cache: (key, value, expires = 0) => {
    let o = {};
    let cached = {
      value: JSON.stringify(value),
      expires: expires ? new Date().getTime() + expires : 0
    };
    o[key] = cached;
    store.dispatch("CACHE_VALUE", o);
  },
  expired(ts) {
    if (ts === 0) return false; // Neer expires
    if (ts < new Date().getTime()) {
      console.log("Object has expired");
      return true;
    } else return false;
  },
  test() {
    console.log("tOTO");
  }
};

export default tvCache;
