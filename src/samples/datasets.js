module.exports = {
  messages: [
    {
      title: "Appointments/Contacts",
      body: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe, quibusdam.",
      emphasis: "32%",
      selected: true
    },
    {
      title: "Monthly vue",
      body:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus dolores earum ipsa, consectetur ipsum odio voluptate adipisci nemo modi magni.",
      emphasis: "45%",
      selected: false
    },
    {
      title: "Daily revenue",
      body:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus dolores earum ipsa, consectetur ipsum odio voluptate adipisci nemo modi magni.",
      emphasis: "45%"
    }
  ],
  gGraphOptions: [
    {
      data: [
        {
          name: "Team 2",
          options: {
            backgroundColor: "rgba(188, 221, 71,0.1)",
            borderColor: "rgb(199, 232, 77)",
            borderWidth: 2,
            pointRadius: 0
          },
          data: generateDummyData("month", 7, 30)
        },
        {
          name: "Team 1",
          options: {
            backgroundColor: "rgba(229, 132, 56, 0.1)",
            borderColor: "rgb(229, 132, 56)",
            borderWidth: 2,
            pointRadius: 0
          },
          data: generateDummyData("month", 7, 30)
        },
        {
          name: "Team 3",
          options: {
            backgroundColor: "rgba(116, 221, 222, 0.1)",
            borderColor: "rgb(116, 221, 222)",
            borderWidth: 2,
            pointRadius: 0
          },
          data: generateDummyData("month", 7, 30)
        },
        {
          name: "Equilibrium",
          options: {
            backgroundColor: "rgba(116, 221, 222, 0.0)",
            borderColor: "grey",
            borderWidth: 1,
            borderDash: [4, 4],
            pointRadius: 0
          },
          data: generateDummyData("line", 7, 17)
        }
      ]
    },
    {
      data: [
        {
          name: "Range 1",
          options: {
            backgroundColor: "rgba(188, 221, 71,0.1)",
            borderColor: "rgb(199, 232, 77)",
            borderWidth: 2,
            pointRadius: 0
          },
          data: generateDummyData("month", 7, 30)
        },
        {
          name: "Range 2",
          options: {
            backgroundColor: "rgba(229, 132, 56, 0.1)",
            borderColor: "rgb(229, 132, 56)",
            borderWidth: 2,
            pointRadius: 0
          },
          data: generateDummyData("month", 7, 30)
        },
        {
          name: "Range 3",
          options: {
            backgroundColor: "rgba(116, 221, 222, 0.1)",
            borderColor: "rgb(116, 221, 222)",
            borderWidth: 2,
            pointRadius: 0
          },
          data: generateDummyData("month", 7, 30)
        }
      ]
    },
    {
      data: [
        {
          name: "Profit",
          options: {
            backgroundColor: "rgba(199, 232, 77, 0.4)",
            borderColor: "rgb(199, 232, 77)",
            borderWidth: 2,
            pointRadius: 0
          },
          data: generateDummyData("month", 7, 30000)
        },
        {
          name: "Loss",
          options: {
            backgroundColor: "rgba(229, 132, 56, 0.4)",
            borderColor: "rgb(229, 132, 56)",
            borderWidth: 2,
            pointRadius: 0
          },
          data: generateDummyData("month", 7, 20000)
        },
        {
          name: "EBITDA",
          options: {
            backgroundColor: "rgba(116, 221, 222, 0.4)",
            borderColor: "rgb(116, 221, 222)",
            borderWidth: 2,
            pointRadius: 0
          },
          data: generateDummyData("month", 7, 15000)
        }
      ]
    }
  ],
  radarData: {
    data: [
      {
        name: "University 1",
        options: {
          backgroundColor: "rgba(199, 232, 77, 0.4)",
          borderColor: "rgb(199, 232, 77)",
          borderWidth: 1,
          pointRadius: 2
        },
        data: [
          { x: "Internationality", y: 10 },
          { x: "Digitalization", y: 12 },
          { x: "Soft Skills", y: 9 },
          { x: "Academic excellence", y: 3 },
          { x: "Relevance", y: 6 },
          { x: "Specialization", y: 10 }
        ]
      },
      {
        name: "University 2",
        options: {
          backgroundColor: "rgba(116, 221, 222, 0.4)",
          borderColor: "rgb(116, 221, 222)",
          borderWidth: 1,
          pointRadius: 2
        },
        data: [
          { x: "Internationalization", y: 11 },
          { x: "Digitalization", y: 8 },
          { x: "Soft Skills", y: 1 },
          { x: "Specialization", y: 9 },
          { x: "Relevance", y: 7 },
          { x: "Specialization", y: 6 }
        ]
      }
    ]
  },
  hColumnsOptions: {
    data: [
      {
        name: "Male",
        options: {
          backgroundColor: "rgba(199, 232, 77, 0.4)",
          borderColor: "rgb(199, 232, 77)",
          borderWidth: 2,
          pointRadius: 0
        },
        data: generateDummyData("numerical", 7, 100, -100)
      },
      {
        name: "Female",
        options: {
          backgroundColor: "rgba(116, 221, 222, 0.4)",
          borderColor: "rgb(116, 221, 222)",
          borderWidth: 2,
          pointRadius: 0
        },
        data: generateDummyData("numerical", 7, 100, -100)
      }
    ]
  },
  bubbleOptions: {
    data: [
      {
        name: "Regional impact",
        options: {
          backgroundColor: "rgba(116, 221, 222, 0.4)",
          borderColor: "rgb(116, 221, 222)"
        },
        data: generateDummyData("3D", 30, 20)
      }
    ]
  }
};

function generateDummyData(type, number, max = 12, min = 0) {
  let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let res = [];
  if (type == "month") {
    for (let index = 0; index < number; index++) {
      let obj = { x: months[index % 12], y: generateRandomNumber(max, min) };
      res.push(obj);
    }
  } else if (type == "line") {
    for (let index = 0; index < number; index++) {
      let obj = { x: months[index % 12], y: max };
      res.push(obj);
    }
  } else if (type == "numerical") {
    for (let index = 0; index < number; index++) {
      let obj = { x: "Value " + index, y: generateRandomNumber(max, min) };
      res.push(obj);
    }
  } else if (type == "3D") {
    for (let index = 0; index < number; index++) {
      let obj = {
        x: generateRandomNumber(max, min),
        y: generateRandomNumber(max, min),
        z: generateRandomNumber(max, min)
      };
      res.push(obj);
    }
  }
  return res;
}

function generateRandomNumber(max, min) {
  return (Math.random() * (max - min) + min).toFixed(2);
}
