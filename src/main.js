import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import "./plugins/fontawesome";
import "./plugins/vueTables.js";
//import "./plugins/globalMixins.js";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./i18n";
import Default from "./layouts/Default";
import NoNav from "./layouts/NoNav";
import Iframe from "./layouts/Iframe";
import { Auth0Plugin } from "./auth";
import config from "@/config/config";

Vue.config.productionTip = false;

Vue.use(Auth0Plugin, {
  domain: config.oauth.domain,
  clientId: config.oauth.clientId,
  onRedirectCallback: appState => {
    router.push(appState && appState.targetUrl ? appState.targetUrl : window.location.pathname);
  }
});

Vue.component("default-layout", Default);
Vue.component("no-nav-layout", NoNav);
Vue.component("iframe-layout", Iframe);

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
