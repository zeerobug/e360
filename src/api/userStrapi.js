import request from "@/api/requestStrapi";
import store from "@/store";

export function login(data) {
  return request({
    url: "/auth/local",
    method: "POST",
    data
  });
}

export function register(data) {
  return request({
    url: "/auth/local/register",
    method: "POST",
    data
  });
}


