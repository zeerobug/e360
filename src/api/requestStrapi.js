/** Ti be used with loopback Backend, no authentication for now, data is public */
import axios from "axios";
import config from "@/config/config";
import store from "@/store";
import tvCache from "@/lib/tvCache";

// create an axios instance
const service = axios.create({
  baseURL: config.STRAPI_URL, // api base_url
  timeout: 20000, // request timeout
  headers: { "Content-Type": "application/json" }
});

// request interceptor
service.interceptors.request.use(
  request => {
    // Check if data has been cached
    // Only for GET method
    if (store.state.token) {
      request.headers["Authorization"] = " Bearer " + store.state.token;
    }
    if (request.method !== "get") return request;

    let resp = tvCache.isCached(tvCache.generateKey(request.url + JSON.stringify(request.params)));
    if (resp) {
      request.data = resp;
      request.adapter = () => {
        return new Promise(resolve => {
          resolve({
            data: resp,
            status: 200,
            statusText: "OK",
            headers: {
              "content-length": resp.length,
              "content-type": "application/json; charset=utf-8"
            },
            config: request,
            request: request
          });
        });
      };
    }
    return request;
  },
  function(error) {
    // Do something with request error
    console.log("Error", error);
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get information such as headers or status
   * Please return  response => response
   */
  //response => response
    response => {
    const res = response.data;
    const code = response.status;
    if (code !== 200 && code !== 204 && code !== 201) {
      return Promise.reject(response);
    } else {
      tvCache.cache(
        tvCache.generateKey(response.config.url + JSON.stringify(response.config.params)),
        res,
        config.cacheExpires
      );
      return res;
    }
  },
    error => {
    if (error.response.status == 401) {
      console.log("Permission problem", error);
      return Promise.reject(error.response);
    } else {
      return Promise.reject(error.response);
    }
  }
);

export default service;
