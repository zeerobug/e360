import request from "@/api/request";
import { prepareData } from "@/lib/utils";
import { defaultUniversities } from "@/config/data";
import { changeProperties, fixDecimal } from "@/lib/utils";
import _ from "lodash";
import axios from "axios";
import config from "@/config/config";
import store from "@/store";

// OBSOLETE
// export function list() {
//   return request({
//     url: "/api/v1/dispatcher/univList",
//     method: "get"
//   });
// }

// OBSOLETE
export function listCountry() {
  return request({
    url: "/api/v1/dispatcher/countryUnivList",
    method: "get"
  });
}

// OBSOLETE
export function rankings(univs) {
  return request({
    url: "/api/v1/dispatcher/getRankings",
    method: "post",
    data: univs
  });
}

// OBSOLETE
export function ranking(univid) {
  return request({
    url: "/api/v1/dispatcher/getRanking?univid=" + univid,
    method: "get"
  });
}

// export function votesDistribution(univid, seg) {
//   return request({
//     url: `/api/v1/dispatcher/votesDistribution?univid=${univid}&seg=${seg}`,
//     method: "get"
//   });
// }

// export async function getMainRegion(univid) {
//   let res = await votesDistribution(univid, "REGION");
//   res["data"] = res["data"].map(fixDecimal);
//   let i = res["data"].reduce((iMax, x, i, arr) => (x > arr[iMax] ? i : iMax), 0);
//   return res["index"][i];
// }

// OBSOLETE
export function votesBySkills(univAr) {
  let univids = univAr.map(obj => obj.id);
  return request({
    url: `/api/v1/dispatcher/votesBySkills`,
    method: "get",
    params: {
      univids: univids
    }
  });
}

// OBSOLETE
export function votedCompetitors(univid) {
  return request({
    url: `/api/v1/dispatcher/votedCompetitors?univid=${univid}`,
    method: "get"
  });
}

// USED Goest to Python backend on NEO4J db

export function similarSkills(univid) {
  return request({
    url: `/api/v1/dispatcher/similarSkills?univid=${univid}`,
    method: "get"
  });
}

// USED consumes Python backend on NEO4J db
export function similarRegion(univid) {
  return request({
    url: `/api/v1/dispatcher/similarRegion?univid=${univid}`,
    method: "get"
  });
}

export function bestAtSkill(skill, data) {
  let q = prepareData(data);
  return request({
    url: `/api/v1/dispatcher/bestAtSkill?skill=${skill}&${q}`,
    method: "get"
  });
}

// USED consumes Python backend on NEO4J db
export function getSkillRank(skill, univid) {
  return request({
    url: `/api/v1/dispatcher/getSkillRank?univid=${univid}&skill=${skill}`,
    method: "get"
  });
}

// OBSOLETE
export function employabilityIndice(univid) {
  return request({
    url: `/api/v1/dispatcher/employabilityIndice?univid=${univid}`,
    method: "get"
  });
}

// OBSOLETE export function getChallengersFromBackEnd(univid) {
//   let userid = "1"; // TODO
//   let filter = {
//     filters: [
//       {
//         col: "univid",
//         opr: "eq",
//         value: univid
//       },
//       {
//         col: "userid",
//         opr: "eq",
//         value: userid
//       }
//     ]
//   };
//   return request({
//     //url: `/api/v1/dispatcher/getChallengers?univid=${univid}`,
//     url: `/api/v1/challengers/`,
//     method: "get",
//     params: {
//       q: filter
//     }
//   });
// }

// OBSOLETE export function saveChallengersToBackend(univid, challengers) {
//   // First do
//   // TODO, find userid from Backend
//   let userid = "1";
//   let url,
//     method = "";
//   return getChallengersFromBackEnd(univid)
//     .then(res => {
//       if (res.count > 0) {
//         url = `/api/v1/challengers/${res.ids[0]}`;
//         method = "put";
//       } else {
//         console.log("ADDING", challengers);
//         url = `/api/v1/challengers/`;
//         method = "post";
//       }
//       return request({
//         url,
//         method,
//         data: {
//           userid,
//           univid,
//           challengers: JSON.stringify(challengers)
//         }
//       });
//     })
//     .catch(e => {
//       console.log("ERROR", e);
//     });
// }

export async function getDefaultChallengers(univid) {
  // Sorbonne
  let res = {};

  if (univid == "5c9e895334cda4f3677e7141") {
    res.type1 = "Pre-selected";
    res.challengersType = ["db"];
    res.challengers = defaultUniversities["Sorbonne"].slice(1);
  }
  // Erasmus
  else if (univid == "5c9e349434cda4f3677e109f") {
    res.type1 = "Pre-selected";
    res.challengersType = ["db"];
    res.challengers = defaultUniversities["Erasmus"].slice(1);
  }
  // Monterrey
  else if (univid == "5c9e7e6234cda4f3677e6cbb") {
    res.type1 = "Pre-selected";
    res.challengersType = ["db"];
    res.challengers = defaultUniversities["Monterrey"].slice(1);
  }
  // epfl
  else if (univid == "5c9e355234cda4f3677e11d1") {
    res.type1 = "Pre-selected";
    res.challengersType = ["db"];
    res.challengers = defaultUniversities["epfl"].slice(1);
  }

  // Other load it from Backend
  else {
    res = await similarSkillsforSettings(univid);
  }
  return res;
}

export async function similarSkillsforSettings(univid) {
  let challengersType, challengers;
  let res = await similarSkills(univid);
  challengersType = ["skills"];
  let univs = _.uniqBy(res, "univid");
  challengers = changeProperties(univs, {
    id: "univid",
    label: "display_name"
  }).slice(1);
  return { challengersType, challengers };
}

export async function sameVotersforSettings(univid, size = 5) {
  let challengers;
  let univs = await votedCompetitors(univid);
  challengers = changeProperties(univs, {
    id: "univid",
    label: "display_name"
  });
  return challengers.slice(0, size);
}

export function uploadForJoiner(file, fieldName) {
  //   return request({
  //     url: `/api/v1/dispatcher/uploadForJoiner?file=${file}`,
  //     method: "post"
  //   });
  let formData = new FormData();
  formData.append("file", file);
  formData.append("fieldName", fieldName);
  let headers = {};
  headers["Authorization"] = " Bearer " + store.state.token;
  headers["Content-Type"] = "multipart/form-data";
  return axios.post(config.API_URL + "/api/v1/dispatcher/uploadForJoiner", formData, {
    headers
  });
}
