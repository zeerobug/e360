import request from "@/api/requestStrapi";

export function getValue(name) {
  let params = {
    name
  };
  return request({
    url: "/questions-2019-s",
    method: "get",
    params
  });
}

export function getNames(params) {
  return request({
    url: "/questions-2019-s/getNames",
    method: "get",
    params
  });
}
