import request from "@/api/requestStrapi";

export function getAccessToken(provider, params) {
  //https://strapi.website.com/auth/github/callback?access_token=eyfvg
  return request({
    url: `/auth/${provider}/callback`,
    method: "get",
    params
  });
}

export function me() {
    //https://strapi.website.com/auth/github/callback?access_token=eyfvg
    return request({
      url: `/users/me`,
      method: "get"
    });
  }