import request from "@/api/requestStrapi";

export function groupedData(params) {
  return request({
    url: "/groupedData",
    method: "get",
    params
  });
}

export function groupedDataMultiple(params) {
  console.log("PARAMS", params);
  return request({
    url: "/groupedDataMultiple",
    method: "get",
    params
  });
}

export function getAverageData(params) {
  return request({
    url: "/averageData",
    method: "get",
    params
  });
}

export function getPercentageData(params) {
  return request({
    url: "/percentageData",
    method: "get",
    params
  });
}

export function rankingDataWithArray(params) {
  return request({
    url: "/rankingDataWithArray",
    method: "get",
    params
  });
}
