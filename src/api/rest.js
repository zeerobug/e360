import request from "@/api/request";

export function endPoints() {
  return request({
    url: "/_openapi",
    method: "GET"
  });
}
