import request from "@/api/request";
import store from "@/store";

export function login(data) {
  return request({
    url: "/api/v1/security/login?remember=True",
    method: "POST",
    data: {
      username: data.username,
      password: data.password,
      provider: "db",
      refresh: true
    }
  });
}

export function refresh() {
  store.dispatch("SET_TOKEN", store.state.refreshToken);
  return request(
    {
      url: "/api/v1/security/refresh",
      method: "POST",
      data: {
        refresh: true
      }
    },
    "refresh"
  );
}

export function userInfo() {
  return request({
    url: "/api/v1/user/userinfo",
    method: "GET"
  });
}

export function loggedIn() {
  return request({
    url: "/api/v1/userhandler/loggedIn",
    method: "GET"
  });
}

export function testProtected() {
  return request({
    url: "/api/v1/userhandler/protected",
    method: "GET"
  });
}

export function notProtected() {
  return request({
    url: "/api/v1/userhandler/notProtected",
    method: "GET"
  });
}

export function reconnect() {
  return new Promise((resolve, reject) => {
    refresh()
      .then(res => {
        if (res.access_token) {
          console.log("REFRESHING", res);
          store.dispatch("SET_TOKEN", res.access_token);
          resolve();
        } else {
          console.log("NO ACCESS TOKEN IN REFRESH");
          logout();
          reject();
        }
      })
      .catch(e => {
        // If error 401, reconnect has failed
        console.log("ERROR", e);
        if (e == 401) logout();
        reject();
      });
  });
}
export function logout() {
  store.dispatch("LOGOUT");
  // Reroute to home
}
