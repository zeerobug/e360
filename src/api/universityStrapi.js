import request from "@/api/requestStrapi";

export function list() {
  return request({
    url: "/universities",
    method: "get"
  });
}

export function fetchList(query) {
  // filter[where][property][regexp]=expression
  const parameters = {
    _start: (query.page - 1) * query.limit,
    _sort: query.sort ? query.sort : "name:ASC",
    _limit: query.limit
  };

  if (query.country !== undefined) {
    parameters["country_contains"] = query.country;
  }
  if (query.query !== undefined && query.query.length >= 3) {
    parameters["name_contains"] = query.query;
  }

  return request({
    url: "/universities",
    method: "get",
    params: parameters
  });
}

export function getUniversityById(id) {
  return request({
    url: "/universities/" + id,
    method: "get"
  });
}

export function fetchRanking(id) {
  return request({
    url: `/ranking-defs?university=${id}`,
    method: "get"
  });
}

export function getChallengersFromBackEnd(userid, univid) {
  let filter = {};
  filter["userUniversity"] = userid + univid;
  return request({
    //url: `/api/v1/dispatcher/getChallengers?univid=${univid}`,
    url: `/challengers`,
    method: "get",
    params: filter
  });
}

export function saveChallengersToBackend(userid, univid, challengers) {
  // First do
  // TODO, find userid from Backend
  let url,
    method = "",
    challengersId = [];
  challengersId = challengers.map(o => o.id);
  return getChallengersFromBackEnd(userid, univid)
    .then(res => {
      if (res.length > 0) {
        url = `/challengers/${res[0].id}`;
        method = "put";
      } else {
        url = `/challengers`;
        method = "post";
      }
      return request({
        url,
        method,
        data: {
          user: userid,
          university: univid,
          userUniversity: userid + univid,
          universities: challengersId
        }
      });
    })
    .catch(e => {
      console.log("ERROR", e);
    });
}
