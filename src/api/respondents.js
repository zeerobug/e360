import request from "@/api/requestStrapi";

// OBSOLETE
export function getFilter(type, table = "q13-2019") {
  return request({
    url: "/respondents/filters/" + type,
    method: "get",
    params: {
      table
    }
  });
}
