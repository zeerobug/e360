import axios from "axios";
import store from "@/store";
import config from "@/config/config";

// create an axios instance
const service = axios.create({
  baseURL: config.API_URL, // api base_url
  timeout: 20000, // request timeout
  headers: { "Content-Type": "application/json" }
});

// request interceptor
service.interceptors.request.use(
  config => {
    // Do something before request is sent
    if (store.state.token) {
      config.headers["Authorization"] = " Bearer " + store.state.token;
    }
    config.headers["Access-Control-Allow-Origin"] = "no";
    return config;
  },
  error => {
    // Do something with request error
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get information such as headers or status
   * Please return  response => response
   */
  //response => response
  response => {
    const res = response.data;
    const code = response.status;
    if (code !== 200 && code !== 204 && code !== 201) {
      return Promise.reject(res.message || "error");
    } else {
      return res;
    }
  },
  error => {
    if (error.response.status == 401) {
      //
      return Promise.reject(error.response.status);
    } else {
      return Promise.reject(error.response.status);
    }
  }
);

export default service;
