/**
 * NEO4J Request wrapper
 *
 */

import axios from "axios";
import config from "@/config/config";
import { objectify } from "@/lib/neo4jUtils";
import store from "@/store";
import tvCache from "@/lib/tvCache";
import _ from "lodash";

// create an axios instance
const service = axios.create({
  baseURL: config.NEO4J_URL, // api base_url
  timeout: 20000, // request timeout
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json;charset=UTF-8",
    Authorization: "Basic " + config.NEO4J_TOKEN
  }
});

// request interceptor
service.interceptors.request.use(
  config => {
    config.headers["Access-Control-Allow-Origin"] = "no";
    return config;
  },
  error => {
    // Do something with request error
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get information such as headers or status
   * Please return  response => response
   */
  response => {
    if (response.data.results.length == 1) {
      const res = objectify(response.data.results[0].data, response.data.results[0].columns);
      const code = response.status;
      if (code !== 200 && code !== 204 && code !== 201) {
        return Promise.reject(res.message || "error");
      } else {
        return res;
      }
    } else {
      console.log("TODO: Multiple results");
    }
  },
  error => {
    if (error.response.status == 401) {
      //
      return Promise.reject(error.response.status);
    } else {
      return Promise.reject(error.response.status);
    }
  }
);

export default service;
