import request from "@/api/neo4j/request";
import _ from "lodash";

export function list() {
  let data = {
    statements: [
      {
        statement:
          "MATCH (u:University)-[:RANKED]-() RETURN distinct u.display_name as label, u.universityId as id"
      }
    ]
  };
  return request({
    url: "/transaction/commit",
    method: "POST",
    data
  });
}

export function listBy(value, property, options = {}) {
  let statement;
  if (property == "COUNTRY") {
    statement = `MATCH (c:Country)<-[:ISIN]-(u:University)-[:RANKED]-() 
           WHERE c["alpha-2"] = "${value.id}"
          RETURN distinct u.display_name as label, u.universityId as id, c.name as country
          ORDER by label`;
  }
  let data = {
    statements: [
      {
        statement: statement
      }
    ]
  };
  return request({
    url: "/transaction/commit?listBy",
    method: "POST",
    data
  });
}

export function listByLocal(parameters, property, options = {}) {
  let statement;
  if (property == "COUNTRY") {
    statement = `MATCH (c:Country)--(r:Respondent {year: {year} })-[v:VOTED]-(u:LocalUniversity)
        WHERE c['alpha-2']={country}
        RETURN DISTINCT u.name as label, u.name as id`;
  }
  let data = {
    statements: [
      {
        statement: statement,
        parameters: {
          country: parameters.country,
          year: parameters.year
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit?listByLocal",
    method: "POST",
    data
  });
}

export function listCountry() {
  let data = {
    statements: [
      {
        statement:
          "MATCH ()-[:RANKED]-(u:University)-[:ISIN]-(c:Country) RETURN distinct c.name as label, c['alpha-2'] as id ORDER BY label"
      }
    ]
  };
  return request({
    url: "/transaction/commit",
    method: "POST",
    data
  });
}

export function rankings(univids = null, numberPerPage = 15, page = 1) {
  let cond = [],
    n = 0,
    limitQ = "";
  if (univids) {
    cond[n++] = "u.universityId IN $univids";
  } else {
    // If not we limit
    let limit = numberPerPage * page;
    let toSkip = numberPerPage * (page - 1);
    limitQ = `SKIP ${toSkip} LIMIT ${limit}`;
  }
  let condition = cond ? "WHERE " + cond.join(" AND ") : "";
  let data = {
    statements: [
      {
        statement: `
                MATCH (u:University)-[rel]-(r:Rank)
                ${condition}
                RETURN DISTINCT rel, u.display_name as display_name, u.universityId as univid, r.year as x, r.rank as y
                ${limitQ}
                `,
        parameters: {
          univids
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit?rankings",
    method: "POST",
    data
  });
}

export function rankingsALL(univids = [], numberPerPage = 15, page = 1) {
  let cond = [],
    n = 0,
    limitQ = "";
  if (univids.length > 0) {
    cond[n++] = "u.universityId IN $univids";
  } else {
    // If not we limit
    let limit = numberPerPage;
    let toSkip = numberPerPage * (page - 1);
    limitQ = `SKIP ${toSkip} LIMIT ${limit}`;
  }
  let condition = cond.length > 0 ? "WHERE " + cond.join(" AND ") : "";
  let data = {
    statements: [
      {
        statement: `
                MATCH (u:University)-[rel]-(r:Rank)
                ${condition}
                RETURN DISTINCT u.display_name as x, u.universityId as univid, collect(r.year)  as year, collect(r.rank) as y
                ${limitQ}
                `,
        parameters: {
          univids
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit?rankingsAll",
    method: "POST",
    data
  });
}

export function ranking(univid) {
  return new Promise((resolve, reject) => {
    rankings(univid).then(res => {
      resolve(res);
    });
  });
}

export function rankingYear(params) {
  let univids = params.filter.COUNTRYUNIV;
  let data = {
    statements: [
      {
        // statement: `
        //         MATCH (r:Respondent {year: {year}})-[v:VOTED {type: "best"}]-(u:University)--(c:Country)
        //         WHERE u.universityId IN $univids
        //         RETURN DISTINCT u.display_name as _id, u.universityId as univid, c.name as y, count(r) as votes ORDER BY votes DESC
        //     `,
        statement: `
                MATCH (rk:Rank {year: {year}})-[:RANKED]-(u:University)--(c:Country)
                WHERE u.universityId IN $univids
                RETURN DISTINCT u.display_name as _id, u.universityId as univid, c.name as y, rk.rank as z ORDER BY z ASC
            `,
        parameters: {
          univids,
          year: params.parameters.year
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit?rankingYear",
    method: "POST",
    data
  });
}

export function rankingDriver(params) {
  let data = {
    statements: [
      {
        statement: `
                MATCH (r:Respondent {year: {year}})-[v:VOTED {type: {driver}}]-(u:University)--(c:Country)
                RETURN DISTINCT u.display_name as _id, u.universityId as univid, c.name as y, count(r) as votes ORDER BY votes DESC LIMIT 50
            `,
        parameters: {
          driver: params.parameters.driver,
          year: params.parameters.year
        }
      }
    ]
  };
  //console.log("QUERY Drivers", data.statements[0]);
  return request({
    url: "/transaction/commit",
    method: "POST",
    data
  });
}

export function votesBySkills(univAr) {
  let univids = univAr.map(obj => obj.id);
  let data = {
    statements: [
      {
        statement: `
MATCH (:Respondent {year: 2020})-[v:VOTED]-(u:University)
WHERE u.universityId  IN $univids AND v.type <> "best"
WITH count(*) as total
MATCH (:Respondent {year: 2020})-[v:VOTED]-(u:University)
WHERE u.universityId  IN $univids AND v.type <> "best"
RETURN DISTINCT v.type as response, u.display_name as display_name, u.universityId as univid, 100.0*count(*)/total as value
            `,
        parameters: {
          univids
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit",
    method: "POST",
    data
  });
}

export function employabilityIndice(univid) {
  let data = {
    statements: [
      {
        statement: `
MATCH (r:Respondent)-[v:VOTED]-(u:University)
WHERE r.year=2020
WITH DISTINCT u.display_name as best, count(r) as votes
ORDER BY votes DESC
LIMIT 1
MATCH (r:Respondent)-[v:VOTED]-(u:University)
WHERE r.year=2020 AND u.universityId=$univid
RETURN 100.0*count(r)/votes as value
            `,
        parameters: {
          univid
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit",
    method: "POST",
    data
  });
}

export function votedCompetitors(univid) {
  let data = {
    statements: [
      {
        statement: `
        
        MATCH (r:Respondent)-[v1:VOTED {type: "best"}]-(u:University)
WHERE u.universityId=$univid
WITH count(r) as total
MATCH (r:Respondent)-[v1:VOTED {type: "best"}]-(u:University)
WHERE u.universityId=$univid
WITH r, total
MATCH (r)-[v:VOTED {type: "best"}]-(u2:University)
WHERE u2.universityId<>$univid
RETURN DISTINCT u2.display_name as display_name, u2.universityId as univid, 100.0*count(r)/total as value
ORDER BY value DESC
LIMIT 10
`,
        parameters: {
          univid
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit",
    method: "POST",
    data
  });
}

export function votesDistribution(univid, seg) {
  let segment;
  if (seg == "REGION") segment = "reg.name";
  else if (seg == "COUNTRY") segment = "c['name']";
  else segment = "r[$seg]";
  let data = {
    statements: [
      {
        statement:
          `
        MATCH (r:Respondent)-[v1:VOTED {type: "best"}]-(u:University)
        WHERE u.universityId=$univid
        WITH count(r) as total
        MATCH (reg:Region)<--(c:Country)<--(r:Respondent)-[v1:VOTED {type: "best"}]-(u:University)
        WHERE u.universityId=$univid
        RETURN DISTINCT ` +
          segment +
          ` as x, 100.0*count(r)/total as y
        ORDER BY x
        `,
        parameters: {
          univid,
          seg
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit?toto",
    method: "POST",
    data
  });
}

export async function getMainRegion(univid) {
  let res = await votesDistribution(univid, "REGION");
  let elt = _.maxBy(res, "y");
  return elt.x;
}
