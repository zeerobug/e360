import request from "@/api/neo4j/request";
import { makeFilter, makeList } from "@/api/neo4j/utils";
import _ from "lodash";

export function rankingDataWithArray(params) {}

export function getFilter(entity) {
  let query;
  // ["COUNTRY", "ROLE", "SECTOR", "COMPANY_SIZE"]
  if (entity == "COUNTRY") {
    query = `MATCH(: Respondent {year:2020})- [: ISIN] - (c: Country)
        RETURN DISTINCT c.name as label, c["alpha-2"] as id
        ORDER BY label
        `;
  } else {
    query = `MATCH(r:Respondent {year:2020})
        RETURN DISTINCT r.${entity} as label, r.${entity} as id
        ORDER BY label
        `;
  }
  let data = {
    statements: [
      {
        statement: query
      }
    ]
  };
  return request({
    url: "/transaction/commit",
    method: "POST",
    data
  });
}

export function getPercentageData(params) {
  let filter;
  filter = params.filter ? makeFilter(params.filter) : "";
  let query;
  if (params.groupField == "COUNTRY") {
    query = `
        MATCH (r:Respondent {year: {year}})--(c:Country)
        ${filter}
        WITH count(*) AS total
        MATCH (r:Respondent {year: {year}})--(c:Country)
        ${filter}
        RETURN c.name as _id, count(*) as count, 100.0*count(*)/total as y
        `;
  } else {
    query = `
        MATCH (r:Respondent {year: {year}})--(c:Country)
        ${filter}
        WITH count(*) AS total
        MATCH (r:Respondent {year: {year}})--(c:Country)
        ${filter}
        RETURN r.${params.groupField} as _id, count(*) as count, 100.0*count(*)/total as y
    `;
  }
  console.log("query", query);
  let data = {
    statements: [
      {
        statement: query,
        parameters: {
          year: Number(params.year)
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit?getPercentageData",
    method: "POST",
    data
  });
}

export function getPercentageDataQuestion(params) {
  let marker = "";
  let filter,
    orderBy = "";
  filter = params.filter ? makeFilter(params.filter) : "";
  let oFilter = filter;
  if (filter == "" && params.addFilter != undefined) {
    oFilter = `WHERE ${params.addFilter} `;
  } else if (params.addFilter != undefined) {
    oFilter = filter + " AND " + params.addFilter;
  }
  if (params.orderBy) orderBy = "ORDER BY " + params.orderBy;
  let query = `
        MATCH (r:Respondent { year: {year} })--(c:Country)
        ${filter}
        WITH count(*) AS total
        MATCH (c:Country)--(r:Respondent {year: {year} })-[an:ANSWERED]-(q:Question {code:{code}})
        ${oFilter}
        RETURN an.value as _id, count(*) as count, 100.0*count(*)/total as y
        ${orderBy}
        `;
  if (params.debug) {
    console.log("DEBUG QUERY", query, "PARAMS", params);
    marker = "DEBUG";
  }
  let data = {
    statements: [
      {
        statement: query,
        parameters: {
          year: Number(params.year),
          code: params.questionId
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit?" + marker,
    method: "POST",
    data
  });
}

export function getPercentageSpecialQuestions(params) {
  /**    MATCH (r:Respondent {year: 2020 })-[an:ANSWERED]-(q:Question)
    WHERE q.code =~ "Q2D_1_.*" OR q.code =~ "Q2D_2_.*"
    RETURN q.text as _id, avg(an.value) as count */
  let filter, addFilter;
  filter = params.filter ? makeFilter(params.filter) : "";
  let filter2 = filter;
  if (params.type == "ranked") {
    addFilter = 'q.code =~ {code} AND (an.value="1st ranked" OR an.value="2nd ranked")';
  } else if (params.type == "approved") {
    addFilter = 'q.code =~ {code} AND an.value="Yes"';
  } else if (params.type == "approved_number") {
    // 2019
    addFilter = 'q.code =~ {code} AND an.value="1.0"';
  } else {
    addFilter = "q.code =~ {code}";
  }
  if (filter2 == "") filter2 = ` WHERE ${addFilter}`;
  else filter2 = filter2 + ` AND ( ${addFilter} ) `;
  let query = `
        MATCH (r:Respondent { year: {year} })--(c:Country)
        ${filter}
        WITH count(*) AS total
        MATCH (c:Country)--(r:Respondent {year: {year} })-[an:ANSWERED]-(q:Question)
        ${filter2}
        RETURN q.text as _id, count(*) as count, 100.0*count(*)/total as y
        `;
  let data = {
    statements: [
      {
        statement: query,
        parameters: {
          year: Number(params.year),
          code: `${params.questionId}.*`
        }
      }
    ]
  };

  return request({
    url: "/transaction/commit?getPercentageSpecialQuestions",
    method: "POST",
    data
  });
}

export function getCustomQuery(params) {
  let filter;
  filter = params.filter ? makeFilter(params.filter) : "";
  let filter2 = filter;
  let order = params.order ? params.order : "";
  if (filter2 == "") filter2 = ` WHERE ${params.addFilter}`;
  else filter2 = filter2 + ` AND ( ${params.addFilter} ) `;
  let query = `
        MATCH (c:Country)--(r:Respondent {year: {year} })-[an:ANSWERED]-(q:Question)
        ${filter2}
        RETURN ${params.result}
        ${order}
        `;
  if (params.debug) {
    console.log("DEBUG", query, "PARAMS", params.parameters);
  }
  let data = {
    statements: [
      {
        statement: query,
        parameters: params.parameters
      }
    ]
  };
  return request({
    url: "/transaction/commit?" + params.marker,
    method: "POST",
    data
  });
}

export function getFullCustomQuery(params) {
  let filter;
  filter = params.filter ? makeFilter(params.filter) : "";
  let filter2 = filter;
  let order = params.order ? params.order : "";
  let limit = params.limit ? params.limit : "";
  if (filter2 == "") filter2 = ` WHERE ${params.addFilter}`;
  else filter2 = filter2 + ` AND ( ${params.addFilter} ) `;
  let query = `
        MATCH (c:Country)--(r:Respondent)-[an:ANSWERED]-(q:Question)
        ${filter2}
        RETURN ${params.result}
        ${order}
        ${limit}
        `;
  if (params.debug) {
    console.log("DEBUG", query, "PARAMS", params.parameters);
  }
  let data = {
    statements: [
      {
        statement: query,
        parameters: params.parameters
      }
    ]
  };
  return request({
    url: "/transaction/commit?" + params.marker,
    method: "POST",
    data
  });
}

export function getRanking(params) {
  console.log("PARAMS", params);
  //   let filter, addFilter;
  //   filter = params.filter ? makeFilter(params.filter) : "";
  //   let filter2 = filter;

  //   if (filter2 == "") filter2 = ` WHERE ${params.addFilter}`;
  //   else filter2 = filter2 + ` AND ( ${params.addFilter} ) `;
  let query = `
    MATCH (r:Respondent)-[v:VOTED {year: {year}}]-()
    WITH count(r) as total
    MATCH (r:Respondent)-[v:VOTED {year: {year}}]-(u:LocalUniversity)
    WHERE u.universityId IN {univs}
    RETURN u.name as _id, 100.0*count(r)/total as o, apoc.number.format(100.0*count(r)/total * 100, '#,##0.00;(#,##0.00)')+"%" as y ORDER BY o DESC
        `;
  if (params.debug) console.log("DEBUG", query, params);
  let data = {
    statements: [
      {
        statement: query,
        parameters: {
          year: params.parameters.year,
          univs: params.filter.UNIV
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit?" + params.marker,
    method: "POST",
    data
  });
}

export function getLocalDriverRanking(params) {
  let query = `
        MATCH (c:Country)--(r:Respondent)-[v:VOTED {year: {year}}]-(u:LocalUniversity)
        WHERE c['alpha-2'] = {country} AND v.type <> 'local'
        RETURN DISTINCT u.name as _id, v.type as driver, count(v) as y ORDER BY y DESC
        `;
  //console.log("QUERY", query, params);
  let data = {
    statements: [
      {
        statement: query,
        parameters: {
          year: params.filter.year,
          country: params.filter.country
        }
      }
    ]
  };
  return request({
    url: "/transaction/commit?currrent",
    method: "POST",
    data
  });
}

/**
 * Use for general response groups. Delivers basic responses
 * @export
 * @param  {object} params
 *                  year: year
 *                  code: question id or regex pattern
 *
 * @return promise results
 */
export function getGeneralResponse(params) {
  let filter = params.filter ? makeFilter(params.filter) : "";
  if (filter !== "") filter = `${filter} AND q.code=~{pattern}`;
  else filter = "WHERE q.code=~{pattern}";
  //console.log("FILTER", filter);
  let query = `
            MATCH (c:Country)--(r:Respondent {year:{year}})-[a:ANSWERED]-(q:Question) ${filter}
            RETURN distinct q.text as _id, a.value as serie, count(*) as y
            `;
  // console.log("QUERY", query, params);
  let data = {
    statements: [
      {
        statement: query,
        parameters: {
          year: params.parameters.year,
          pattern: params.parameters.pattern
        }
      }
    ]
  };
  // console.log("Query", data.statements[0]);
  return request({
    url: "/transaction/commit?getGeneralReponse",
    method: "POST",
    data
  });
}
