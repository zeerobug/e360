import _ from "lodash";

export function makeFilter(filterData) {
  /*
    COMPANY_SIZE: undefined;
    ROLE: "Corporate/Business roles";
    SECTOR: "Banking and insurance";
    */
  let filter = [];
  let filterStr = "";
  try {
    Object.keys(filterData).forEach(f => {
      let key,
        ent = "";
      if (f == "COUNTRY") {
        key = "alpha-2";
        ent = "c";
      } else {
        key = f;
        ent = "r";
      }
      if (filterData[f] !== undefined) {
        if (_.isArray(filterData[f])) {
          let member = "[" + filterData[f].map(m => `"${m}"`) + "]";
          filter.push(`${ent}["${key}"] IN ${member}`);
        } else filter.push(`${ent}["${key}"] = "${filterData[f]}"`);
      }
    });
    if (filter.length > 0) filterStr = " WHERE " + filter.join(" AND ");
    return filterStr;
  } catch (e) {
    console.log(e);
  }
}

export function copleteQuery(options) {
  let limit, order;
  if (options.limit) {
    limit = "";
  }
}

export function makeList(elts) {
  let res = elts.map(o => {
    return o.id;
  });
  return res;
}
