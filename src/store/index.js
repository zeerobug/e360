import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    loggedIn: true,
    currentBroadcast: null,
    token: null,
    refreshToken: null,
    userData: null,
    interface: {},
    tableLoading: false,
    connectorArguments: {},
    cache: {}, // Cache is clearable
    parameters: {}, // parameters are not
    contactFormFilled: false,
    contactFormClosed: false,
    popup_viewed: new Date().getTime()
  },
  getters: {
    getConnectorArguments: state => {
      return state.connectorArguments;
    },
    allCached: state => {
      return state.cache;
    },
    getCached: state => key => {
      return state.cache.find(o => (o ? o.key === key : null));
    }
  },
  mutations: {
    TOKEN: (state, { token }) => {
      state.token = token;
    },
    REFRESH_TOKEN: (state, { token }) => {
      state.refreshToken = token;
    },
    USER_DATA: (state, data) => {
      state.userData = data;
    },
    LOGOUT: state => {
      state.userData = null;
      state.token = null;
      state.loggedIn = false;
    },
    LOGIN: state => {
      state.loggedIn = true;
    },
    SET_INTERFACE: (state, data) => {
      Object.assign(state.interface, data);
    },
    SET_CONNECTOR_ARGUMENTS: (state, data) => {
      Object.assign(state.connectorArguments, data);
    },
    REMOVE_CONNECTOR_ARGUMENTS: state => {
      state.connectorArguments = {};
    },
    CACHE_VALUE: (state, data) => {
      Object.assign(state.cache, data);
    },
    SAVE_VALUE: (state, data) => {
      Object.assign(state.parameters, data);
    },
    CLEAR_CACHE: state => {
      state.cache = {};
    },
    POPUP_VIEWED: (state, now) => {
      state.popup_viewed = now;
    },
    CONTACT_FORM_FILLED: state => {
      state.contactFormFilled = true;
    }
  },
  actions: {
    SET_INTERFACE: function({ commit }, obj) {
      commit("SET_INTERFACE", obj);
    },
    SET_TOKEN: function({ commit }, token) {
      commit("TOKEN", { token });
    },
    SET_USER: function({ commit }, userData) {
      commit("USER_DATA", userData);
    },
    LOGIN: function({ commit }) {
      commit("LOGIN");
    },
    LOGOUT: function({ commit }) {
      // I also have to logout from server
      //   logout().then(res => {
      //     console.log(res);
      //   });
      commit("LOGOUT");
    },
    SET_CONNECTOR_ARGUMENTS: function({ commit }, payload) {
      commit("SET_CONNECTOR_ARGUMENTS", payload);
    },
    REMOVE_CONNECTOR_ARGUMENTS: function({ commit }, name) {
      commit("REMOVE_CONNECTOR_ARGUMENTS", name);
    },
    CACHE_VALUE: function({ commit }, payload) {
      commit("CACHE_VALUE", payload);
    },
    SAVE_VALUE: function({ commit }, payload) {
      commit("SAVE_VALUE", payload);
    },
    CLEAR_CACHE: function({ commit }) {
      commit("CLEAR_CACHE");
    },
    EXIT_POPUP_VIEWED: function({ commit }, now) {
      commit("POPUP_VIEWED", now);
    },
    CONTACT_FORM_FILLED: function({ commit }) {
      commit("CONTACT_FORM_FILLED");
    }
  }
});
