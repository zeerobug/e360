/** 
 * Message structure
 *  let model = {
        title: "Message 3",
        body:
            "Quaerat iusto dolores saepe voluptatem odio suscipit provident quibusdam voluptates nam doloribus totam natus inventore similique fuga, necessitatibus in ratione excepturi. At.",
        emphasis: "98",
        selected: false
    };
 */
import Message from "@/lib/Message";
import VotedCompetitors from "@/components/charts/VotedCompetitors";
import SimilarSkills from "@/components/charts/SimilarSkills";
import SimilarRegion from "@/components/charts/SimilarRegion";
import { votesBySkills, getMainRegion } from "@/api/neo4j/university";

export default {
  methods: {
    clicked(value) {
      alert(value);
    },
    async getMessages(data) {
      let messages = [];

      if (data.type == "skills") {
        // we fetch
        let res = await votesBySkills([data]);
        const max = res.reduce((prev, current) => (prev.value > current.value ? prev : current)); //returns object
        messages.push(
          new Message({
            title: "Drivers siblings",
            body: `Discover which other insitution have been voted following a similar profile`,
            link: "votedCompetitors/" + data.id,
            returnFocus: "#rank",
            component: SimilarSkills
          })
        );
        messages.push(
          new Message({
            title: "Best of class",
            body: `Your principal driver is "${max.response}" Which other insitution have the same?`,
            link: "skill/" + max.response
          })
        );
      } else if (data.type == "segment") {
        // We get the maximum
        let region = await getMainRegion(data.id);
        messages.push(
          new Message({
            title: "Votes region origin",
            body: `The votes for ${data.name} came mainly from ${region}. Which insitutions have a similar distribution?`,
            link: "votedCompetitors/" + data.univid,
            component: SimilarRegion
          })
        );
      } else if (data.type == "rank") {
        messages.push(
          new Message({
            title: "Competitors study",
            body:
              "Wich other universities did your voters vote for? Click here to display the study",
            link: "votedCompetitors/" + data.univid,
            component: VotedCompetitors,
            returnFocus: "#rank"
          })
        );
      }

      return messages;
    }
  }
};
