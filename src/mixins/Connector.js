import ConnectorEventBus from "./ConnectorEventBus";
import conStore from "./ConnectorStore";
import store from "../store";

const Connector = {
  data() {
    return {
      useVuex: false,
      con: ConnectorEventBus,
      debugConnector: name,
      caller: this.$options.name ? `${this.$options.name} (${this._uid})` : `${this._uid}`
    };
  },
  methods: {
    connectorSubscribe(name, cb) {
      this.debugConnector ? this.message("subscribe", name) : null;
      conStore.addReceiver(name, this.caller);
      this.con.$on(name, res => {
        // console.log("SUBSCRIBING", name);
        cb(res, name);
      });
    },
    connectorPublish(name, value) {
      this.debugConnector ? this.message("publish", name) : null;
      conStore.addEmitter(name, this.caller);
      let payload = {};
      payload[name] = value;
      store.dispatch("CACHE_VALUE", payload);
      // console.log("PUBLISHING", name);
      this.con.$emit(name, value);
    },
    connectorUnsubscribe(name) {
      this.con.$off(name);
    },
    message(type, name) {
      let msg = "";
      if (type === "subscribe") msg = `The component ${this.caller} is Subscribed to ${name}`;
      else if (type === "publish") msg = `The component ${this.caller} has published to ${name}`;
    },
    listConnections() {
      conStore.listConnections();
    }
  }
};

export default Connector;
