import Cojasclib from "cojasclib";
import graphUtils from "@/lib/graphUtils";
import { hasValues, formatPercent, getAveragePoints } from "@/lib/utils";
import { splitText, formatPercentNumber, renameProperty, filtersId, sortY } from "@/lib/utils";
import _ from "lodash";
import { getNames } from "../api/questionsStrapi";

const ChartHelper = {
  mounted() {},
  data() {
    return {
      gu: new graphUtils(),
      graphOptions: {
        // title: {
        //   display: true,
        //   text: this.subtitle
        // },
        defaultFontColor: "grey",
        legendDisplay: true,
        xDisplay: true,
        xDisplayGrid: true,
        yDisplay: true,
        legend: {
          display: true,
          position: "top",
          fullWidth: false,
          labels: {
            fontColor: "grey",
            padding: 20,
            fontSize: 11,
            boxWidth: 10
          }
        },
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true
              }
            }
          ],
          xAxes: [
            {
              ticks: {
                beginAtZero: true
              }
            }
          ]
        }
      }
    };
  },
  methods: {
    export2Image(canvas, title) {
      if (canvas == undefined) {
        console.log("No canvas for ", title);
        return;
      }
      let img = canvas.toDataURL("image/png");
      let link = document.createElement("a");
      link.download = title;
      link.href = img;
      link.click();
    },
    /**
     *
     * @param  {any} filter
     * @param  {any} options
     * @return {void}
     */
    async fetchData(filter, options = {}, origin = "") {
      this.visible = false;
      this.chart = new Cojasclib.Chart(this.graphOptions);
      let selectionFilter;
      selectionFilter = this.makeFilter(filter, options);
      if (!options.postProcess) {
        console.log("Error for ", options);
        throw new Error("missing post process function name for prefix" + options);
      }
      if (_.isFunction(options.postProcess)) {
        options.postProcess(selectionFilter, options, origin);
      } else {
        // Avoid adding methods here, now send in options if it will be used only once
        if (options.postProcess == "singleAverage")
          this.singleAverageProcess(selectionFilter, options);
        else if (options.postProcess == "groupedDataMultiple")
          this.groupedDataMultiple(selectionFilter, options);
        else if (options.postProcess == "multipleAverage")
          this.multipleAverageProcess(selectionFilter, options);
        else if (options.postProcess == "multiFilterAverage")
          this.multiFilterAverage(selectionFilter, options, origin);
        else if (options.postProcess == "singleFilterAverage")
          this.singleFilterAverage(selectionFilter, options, origin);
        else if (options.postProcess == "multiFilterAverage2")
          this.multiFilterAverage2(selectionFilter, options, origin);
        else if (options.postProcess == "simple") this.simple(selectionFilter, options, origin);
      }
    },

    /**
     *
     * @param  {any} selectionFilter
     * @param  {any} options
     * @return {void}
     */
    async simple(selectionFilter, options) {
      let args = _.clone(options.dataFunctionArgs);
      if (options.dimension) {
        args.groupField = options.dimension;
      }

      let data = await options.dataFunction({
        ...args,
        filter: selectionFilter
      });

      let palette = this.gu.largePalette;
      this._addSerie(options.serieName, data, palette);
      this.render(options.renderOptions);
    },

    /**
     *
     * @param  {any} selectionFilter
     * @param  {any} options
     * @return {void}
     */
    async singleAverageProcess(selectionFilter, options) {
      let world = [];

      if (options.allSerie) {
        // All is the same function not grouped
        let argsAll = _.clone(options.dataFunctionArgs);
        argsAll.groupField = "";
        world = await options.dataFunction(argsAll);
        world[0]._id = "All";
        world[0].options = {
          color: this.gu.palette_strong[0]
        };
        if (options.formatY) {
          world.forEach(elt => (elt["y"] = formatPercentNumber(elt["y"])));
        }
      }
      let args = _.clone(options.dataFunctionArgs);
      if (options.dimension) {
        args.groupField = options.dimension;
      }
      let res = await options.dataFunction({
        ...args,
        filter: selectionFilter
      });
      if (options.formatY) {
        res.forEach(elt => (elt["y"] = formatPercentNumber(elt["y"])));
      }
      // Emit values if needed
      if (options.emitData) {
        let resE = _.cloneDeep(res);
        if (options.emitData.sort) {
          resE = sortY(resE, options.emitData);
          if (options.emitData.addRank) {
            resE.map((elt, i) => (elt.r = i + 1));
          }
        }
        this.connectorPublish(options.subscriberPrefix + "Data", resE);
      }
      let palette = [...Array(res.length + 1).fill(this.gu.palette_strong[1])];
      this._addSerie(options.serieName, [...world, ...res], palette, { layer: 2 });
      this.render(options.renderOptions);
    },
    async singleFilterAverage(selectionFilter, options, origin) {
      let points = [];
      // ALL Serie
      // All is the same function not grouped
      let argsAll = _.cloneDeep(options.dataFunctionArgs);
      argsAll.groupField = "";
      points = await options.dataFunction({ ...argsAll, filter: selectionFilter });
      points[0]._id = "All";
      points[0].options = {
        color: this.gu.palette_strong[0]
      };
      points[0].y = getAveragePoints(points);
      if (options.formatY) {
        points.forEach(elt => (elt["y"] = elt["y"].toFixed(2)));
      }
      let palette = [...Array(points.length + 1).fill(this.gu.palette_strong[1])];
      this.graphOptions.legend.display = false; // Bad hack, should be in options
      this._addSerie(options.serieName, points, palette);
      this.render(options.renderOptions);
    },
    async multiFilterAverage(selectionFilter, options, origin) {
      let points = [];

      if (options.allSerie) {
        // All is the same function not grouped
        let argsAll = _.clone(options.dataFunctionArgs);
        argsAll.groupField = "";
        points = await options.dataFunction(argsAll);
        points[0]._id = "All";
        points[0].options = {
          color: this.gu.palette_strong[0]
        };
      }
      //let multiFilter = this.generateMultiFilter(selectionFilter, origin, options)
      for (let i = 0; i < options.filtersNumber; i++) {
        let args = _.clone(options.dataFunctionArgs);
        if (options.dimension) {
          args.groupField = options.dimension;
        }
        let res = await options.dataFunction({
          ...args,
          filter: _.isArray(selectionFilter) ? selectionFilter[i] : ""
        });
        res[0]._id = "Group " + (i + 1);
        points.push(res[0]);
      }
      if (options.formatY) {
        points.forEach(elt => (elt["y"] = formatPercentNumber(elt["y"])));
      }
      let palette = [...Array(points.length + 1).fill(this.gu.palette_strong[1])];
      this.graphOptions.legend.display = false; // Bad hack, should be in options
      this._addSerie(options.serieName, points, palette);
      this.render(options.renderOptions);
    },
    async multiFilterAverage2(selectionFilter, options, origin) {
      let points = [];
      let palette = this.gu.palette;
      let interc = {};
      if (options.allSerie) {
        // All is the same function not grouped
        let argsAll = _.clone(options.dataFunctionArgs);
        points = await options.dataFunction(argsAll);
        if (options.interchange) interc.all = points;
        else {
          // Format y for percentage
          if (options.formatY) {
            points.forEach(elt => (elt["y"] = formatPercent(elt["y"], 2, false, "")));
          }
          if (points) {
            this._addSerie("All (%)", points, Array(points.length + 1).fill(palette[0]));
          } else {
            console.log("Empty serie");
          }
        }
      }

      //let multiFilter = this.generateMultiFilter(selectionFilter, origin, options)
      if (!options.filtersNumber || options.filtersNumber == 0) options.filtersNumber = 1;
      for (let i = 0; i < options.filtersNumber; i++) {
        let args = _.clone(options.dataFunctionArgs);
        if (options.dimension) {
          args.groupField = options.dimension;
        }
        let res = await options.dataFunction({
          ...args,
          filter: _.isArray(selectionFilter) ? selectionFilter[i] : ""
        });
        if (options.interchange) interc["Group " + (i + 1)] = res;
        else {
          if (options.formatY) {
            res.forEach(elt => (elt["y"] = formatPercent(elt["y"], 2, false, "")));
          }
          // let palette = [...Array(points.length + 1).fill(this.gu.normalPalette[i].borderColor)];
          if (hasValues(selectionFilter[i])) {
            this._addSerie(
              options.serieName ? options.serieName : "Group" + " " + (i + 1) + " (%)",
              res,
              Array(points.length + 1).fill(palette[i + 1])
            );
          }
        }
      }
      if (options.interchange) this.interchange(interc, palette);
      this.render(options.renderOptions);
    },

    /**
     *
     * @param  {any} selectionFilter
     * @param  {any} options
     * @return {void}
     */
    async multipleAverageProcess(selectionFilter, options) {
      let all = [];
      let filter = {};
      if (!options.allSerie) {
        filter = selectionFilter;
      }
      // All is the same function not grouped
      let argsAll = _.clone(options.dataFunctionArgs);
      argsAll.groupField = options.dimension;
      all = await options.dataFunction({ ...argsAll, filter });
      let palette = [];
      //let res = divideIntoSeries(all, options.dataFunctionArgs.averageField);
      let n = 1;
      let names = null;
      if (options.translateQuestions)
        names = await getNames({ values: options.dataFunctionArgs.averageField });

      options.dataFunctionArgs.averageField.forEach(field => {
        let mapper = {};
        mapper[field] = "y";
        let ar = renameProperty(all, mapper);
        palette = [...Array(_.values(ar).length).fill(this.gu.palette_strong[n++])];
        this._addSerie(names.find(o => o.value == field).name, ar, palette);
      });

      let args = _.clone(options.dataFunctionArgs);
      if (options.dimension) {
        args.groupField = options.dimension;
      }

      /*
      let res = await options.dataFunction({
        ...args,
        filter: selectionFilter
      });
      let palette = [...Array(res.length + 1).fill(this.gu.palette_strong[1])];
      this._addSerie(options.serieName, [...world, ...res], palette, { layer: 2 });
       */
      this.render(options.renderOptions);
    },

    /**
     *
     * @param  {any} selectionFilter
     * @param  {any} options
     * @return {void}
     */
    async groupedDataMultiple(selectionFilter, options) {
      let args = _.clone(options.dataFunctionArgs);
      if (options.dimension) {
        args.groupField = options.dimension;
      }
      let valNumber = 0;
      // Serie ALl
      if (options.allSerie) {
        valNumber = await this._process_serie({}, args, valNumber, "All", options, 0);
      }

      // Series 1
      await this._process_serie(selectionFilter, args, valNumber, options.serieName, options, 1);
      // Then for each added series, we generate a new serie
      for (let i = 1; i < options.groups; i++) {
        await this._process_serie(
          selectionFilter,
          args,
          valNumber,
          options.serieName + " " + (i + 1),
          options,
          2
        );
      }
      this.render(options.renderOptions);
    },
    async _process_serie(filter, args, valNumber, name, options, order) {
      let newRes = await options.dataFunction({
        ...args,
        filter
      });
      if (options.translateQuestions) newRes = await this.translateQuestions(newRes);
      if (options.formatY) {
        newRes.forEach(elt => (elt["y"] = formatPercentNumber(elt["y"])));
      }
      if (valNumber == 0) valNumber = newRes.length;
      let palette = [...Array(valNumber).fill(this.gu.palette_strong[order])];
      this._addSerie(name, newRes, palette);
      return valNumber;
    },
    /**
     *
     * @param  {any} name
     * @param  {any} res
     * @param  {any} backgroundColor
     * @param  {any} more
     * @return {void}
     */
    _addSerie(name, res, backgroundColor, more) {
      // Prepare points
      if (!_.isArray(res)) return;
      res.map(p => (p._id ? (p.x = p._id) : p.x));
      let serie = new Cojasclib.Serie({
        name: name,
        data: res,
        options: {
          backgroundColor,
          ...more,
          ...this.serieOptions
        }
      });
      this.chart.setSerie(serie);
    },
    render(renderOptions) {
      let plugin = renderOptions.plugin ? renderOptions.plugin : "chartjs";
      this.chart.render(plugin, renderOptions).then(data => {
        this.chartData = data[0];
        this.options = data[1];
        // Here we pass parameters from renderOptions directly to chart component
        if (renderOptions.stacked) {
          _.set(this.options, "scales.xAxes[0].stacked", true);
          _.set(this.options, "scales.yAxes[0].stacked", true);
        }
        this.options.title = renderOptions.title;
        this.visible = true;
      });
    },

    subscribeAll(prefix, filtersNumber) {
      this.subscribeToFilters(prefix);
      for (let i = 0; i < filtersNumber; i++) {
        let names = filtersId(prefix, filtersNumber);
        this.subscribeToFilters(names[i]);
      }
    },
    async subscribeToFilters(prefix) {
      this.connectorSubscribe(prefix + "COUNTRY", async data => {
        console.log("SUS", prefix + "COUNTRY", data);
        this.fetchData({ COUNTRY: data }, this.fetchArgs, prefix);
      });
      this.connectorSubscribe(prefix + "COUNTRYUNIV", async data => {
        this.fetchData({ COUNTRYUNIV: data }, this.fetchArgs, prefix);
      });
      this.connectorSubscribe(prefix + "ROLE", async data => {
        this.fetchData({ ROLE: data }, this.fetchArgs, prefix);
      });
      this.connectorSubscribe(prefix + "SECTOR", async data => {
        this.fetchData({ SECTOR: data }, this.fetchArgs, prefix);
      });
      this.connectorSubscribe(prefix + "UNIV", async data => {
        this.fetchData({ UNIV: data }, this.fetchArgs, prefix);
      });
      this.connectorSubscribe(prefix + "EMPLOYEE_NUMBER", async data => {
        this.fetchData({ EMPLOYEE_NUMBER: data }, this.fetchArgs, prefix);
      });
      //userProfile is a Dimension
      this.connectorSubscribe(prefix + "userProfile", async data => {
        //this.fetchArgs.dataFunctionArgs.groupField = data.id; // Here it gets common to all
        this.fetchArgs.dimension = data.id;
        this.fetchData({}, this.fetchArgs, prefix);
      });
    },
    getCountries(prefix) {
      return this.$store.state.cache[prefix + "country"]
        ? this.$store.state.cache[prefix + "country"].map(obj => obj.id)
        : ["All"];
    },
    async translateQuestions(res) {
      let q = [];
      // Contruct query
      res.forEach(elt => {
        q.push(elt.label);
      });
      let nameAr = await getNames({ values: q });
      res.forEach(v => {
        let e = nameAr.find(o => o.value == v.label);
        v.label = splitText(e.name);
        v.x = e.name;
      });
      return res;
    },
    //   generateMultiFilter(filter, origin, options) {
    //       console.log("MULTI", filter, origin);
    //       let ret = []
    //         let number = origin.split("_##_")[1]
    //         ret[number] = filter
    //       return ret;
    // }
    interchange(points, palette) {
      let res = {};
      Object.keys(points).forEach(key => {
        points[key].forEach(p => {
          let np = _.clone(p);
          np._id = key;
          res[p._id] ? res[p._id].push(np) : (res[p._id] = [np]);
        });
      });
      let i = 0;
      for (let key in res) {
        this._addSerie(key, res[key], Array(res[key].length + 1).fill(palette[i + 1]));
        i++;
      }
    }
  }
};

export default ChartHelper;
