import _ from "lodash";
import { filtersId, filtersDefinition } from "@/lib/utils";

const Neo4jelper = {
  methods: {
    makeFilter(otherFilter, options) {
      let filterNames = filtersDefinition();
      let filtersNumber = options.filtersNumber;
      let prefix = options.subscriberPrefix;
      let res;
      if (filtersNumber == 0 || filtersNumber == undefined) {
        let filtersRes = this.generateFilter(this.$store.state.cache, prefix, filterNames);
        otherFilter = otherFilter ? this.generateFilter(otherFilter, prefix, filterNames) : {};
        res = { ...filtersRes, ...otherFilter };
      } else {
        res = [];
        let names = filtersId(prefix, filtersNumber);
        for (let i = 0; i < filtersNumber; i++) {
          let filtersRes = this.generateFilter(this.$store.state.cache, names[i], filterNames);
          res[i] = { ...filtersRes };
        }
      }

      if (!res && options.defaultFilter) {
        res = this.generateFilter(options.defaultFilter, "", filterNames);
      }
      return res;
    },
    generateFilter(ar, prefix, filterNames) {
      let filtersRes = {};
      for (let i = 0; i < filterNames.length; i++) {
        const filterName = prefix + filterNames[i];
        let value = ar[filterName];
        if (value && value.length > 0) {
          if (value.length > 1) {
            filtersRes[filterNames[i]] = value.map(o => o.id);
            // filtersRes["$or"] = filtersRes["$or"] ? filtersRes["$or"] : [];
            // value.forEach(v => {
            //   let obj = {};
            //   obj[filterNames[i]] = v.id;
            //   filtersRes["$or"].push(obj);
            // });
          } else if (value.length == 1) {
            filtersRes[filterNames[i]] = value[0].id;
          }
        } else if (_.isObject(value)) {
          filtersRes[filterNames[i]] = value.id;
        }
      }
      return filtersRes;
    }
    // makeCondition(filters, options) {
    //   console.log("FILTERS", filters);
    // }
  }
};

export default Neo4jelper;
