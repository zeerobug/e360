var store = {
  debug: true,
  state: {
    message: "Hello!",
    emitters: [],
    receivers: [],
    cache: {}
  },
  addEmitter(name, component) {
    this.saveConnection("emitters", { name, component });
  },
  addReceiver(name, component) {
    this.saveConnection("receivers", { name, component });
  },
  exists(ar, elt) {
    return ar.some(o => o.name == elt.name && o.component == elt.component);
  },
  listConnections() {
    console.log("Publishers", this.state.emitters);
    console.log("Subscribers", this.state.receivers);
  },
  saveConnection(type, connector) {
    this.state[type].push(connector);
  }
};

export default store;
