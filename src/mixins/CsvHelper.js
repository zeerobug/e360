const CsvHelper = {
  mounted() {
    this.$root.$on("export_csv", data => {
      if (!this.id) console.log("ID missing for chart " + data[1]);
      if (this.id == data[0]) {
        this.prepareCSV(data[1]);
      }
    });
  },
  methods: {
    async prepareCSV(title) {
      if (this.chart !== undefined && this.chart.render instanceof Function) {
        const data = await this.chart.render("csvPlugin", {
          horizontal: false
        });
        this.download(data, title + ".csv");
      }
    },
    download(text, filename) {
      var element = document.createElement("a");
      element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
      element.setAttribute("download", filename);
      element.style.display = "none";
      document.body.appendChild(element);
      element.click();
      document.body.removeChild(element);
    }
  }
};

export default CsvHelper;
